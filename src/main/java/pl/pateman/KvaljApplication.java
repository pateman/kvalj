package pl.pateman;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication()
public class KvaljApplication {
	public static void main(String[] args) {
		SpringApplication.run(KvaljApplication.class, args);
	}
}

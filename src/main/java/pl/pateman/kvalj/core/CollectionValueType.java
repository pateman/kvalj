package pl.pateman.kvalj.core;

/**
 * Created by pateman.
 */
public enum CollectionValueType {
    STRING,
    NUMERIC,
    FLOATING_POINT,
    STRING_LIST,
    STRING_SET
}

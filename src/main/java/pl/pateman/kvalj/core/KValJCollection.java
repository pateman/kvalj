package pl.pateman.kvalj.core;

import java.util.Collection;

/**
 * Created by pateman.
 */
public interface KValJCollection<VALUE> {
    VALUE put(final String key, final String value);
    VALUE get(final String key);
    VALUE remove(final String key);
    VALUE append(final String key, final String value);

    int size();
    Collection<String> keys();
}

package pl.pateman.kvalj.core.impl.collection;

import pl.pateman.kvalj.core.impl.AbstractKValJAtomicTypeCollection;

/**
 * Created by pateman.
 */
public class NumericCollection extends AbstractKValJAtomicTypeCollection<Long> {
    @Override
    protected Long valueFromString(String value) {
        try {
            return Long.parseLong(value);
        } catch (NumberFormatException nfe) {
            throw new IllegalStateException("The given value is not a valid number");
        }
    }
}

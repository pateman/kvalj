package pl.pateman.kvalj.core.impl.collection;

import pl.pateman.kvalj.core.impl.AbstractKValJAtomicTypeCollection;

/**
 * Created by pateman.
 */
public final class StringCollection extends AbstractKValJAtomicTypeCollection<String> {

    @Override
    protected String valueFromString(String value) {
        return value;
    }
}

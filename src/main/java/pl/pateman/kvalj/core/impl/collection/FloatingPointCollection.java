package pl.pateman.kvalj.core.impl.collection;

import pl.pateman.kvalj.core.impl.AbstractKValJAtomicTypeCollection;

/**
 * Created by pateman.
 */
public class FloatingPointCollection extends AbstractKValJAtomicTypeCollection<Double> {
    @Override
    protected Double valueFromString(String value) {
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException nfe) {
            throw new IllegalStateException("The given value is not a valid floating-point number");
        }
    }
}

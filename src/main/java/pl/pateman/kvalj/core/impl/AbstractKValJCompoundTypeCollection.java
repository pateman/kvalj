package pl.pateman.kvalj.core.impl;

import org.apache.commons.lang3.StringUtils;
import pl.pateman.kvalj.core.KValJCollection;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by pateman.
 */
public abstract class AbstractKValJCompoundTypeCollection<VALUE> implements KValJCollection<Collection<VALUE>> {
    private final Map<String, Collection<VALUE>> collection;

    public AbstractKValJCompoundTypeCollection() {
        this.collection = new ConcurrentHashMap<>();
    }

    protected abstract Collection<VALUE> createCollection();
    protected abstract VALUE valueFromString(final String value);

    protected Collection<VALUE> getValuesFromString(final String value) {
        final Collection<VALUE> valueCollection = this.createCollection();
        if (StringUtils.isBlank(value)) {
            return valueCollection;
        }

        final List<VALUE> valueList = Arrays.
                stream(value.split(",")).
                map(this::valueFromString).
                collect(Collectors.toList());
        valueCollection.addAll(valueList);
        return valueCollection;
    }

    @Override
    public Collection<VALUE> put(String key, String value) {
        return this.collection.compute(key, (k, v) -> {
            v = this.createCollection();

            final Collection<VALUE> valuesFromString = this.getValuesFromString(value);
            v.addAll(valuesFromString);

            return v;
        });
    }

    @Override
    public Collection<VALUE> get(String key) {
        return this.collection.get(key);
    }

    @Override
    public Collection<VALUE> remove(String key) {
        return this.collection.remove(key);
    }

    @Override
    public Collection<VALUE> append(String key, String value) {
        return this.collection.compute(key, (k, v) -> {
            if (v == null) {
                v = this.createCollection();
            }

            final Collection<VALUE> valuesFromString = this.getValuesFromString(value);
            v.addAll(valuesFromString);

            return v;
        });
    }

    @Override
    public final int size() {
        return this.collection.size();
    }

    @Override
    public final Collection<String> keys() {
        return new HashSet<>(this.collection.keySet());
    }

}

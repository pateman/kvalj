package pl.pateman.kvalj.core.impl.collection;

import pl.pateman.kvalj.core.impl.AbstractKValJCompoundTypeCollection;

import java.util.Collection;
import java.util.HashSet;

/**
 * Created by pateman.
 */
public class StringSetCollection extends AbstractKValJCompoundTypeCollection<String> {
    @Override
    protected Collection<String> createCollection() {
        return new HashSet<>();
    }

    @Override
    protected String valueFromString(String value) {
        return value;
    }
}

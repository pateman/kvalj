package pl.pateman.kvalj.core.impl;

import pl.pateman.kvalj.core.KValJCollection;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by pateman.
 */
public abstract class AbstractKValJAtomicTypeCollection<VALUE> implements KValJCollection<VALUE> {
    private final Map<String, VALUE> collection;

    public AbstractKValJAtomicTypeCollection() {
        this.collection = new ConcurrentHashMap<>();
    }

    @Override
    public VALUE put(String key, String val) {
        final VALUE valueFromString = this.valueFromString(val);
        return this.collection.put(key, valueFromString);
    }

    @Override
    public VALUE get(String key) {
        return this.collection.get(key);
    }

    @Override
    public VALUE remove(String key) {
        return this.collection.remove(key);
    }

    @Override
    public VALUE append(String key, String value) {
        throw new UnsupportedOperationException("Incorrect operation for an atomic type collection");
    }

    @Override
    public final int size() {
        return this.collection.size();
    }

    @Override
    public final Collection<String> keys() {
        return new HashSet<>(this.collection.keySet());
    }

    protected abstract VALUE valueFromString(final String value);
}

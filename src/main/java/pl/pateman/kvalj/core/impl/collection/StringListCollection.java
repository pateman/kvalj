package pl.pateman.kvalj.core.impl.collection;

import pl.pateman.kvalj.core.impl.AbstractKValJCompoundTypeCollection;

import java.util.Collection;
import java.util.LinkedList;

/**
 * Created by pateman.
 */
public class StringListCollection extends AbstractKValJCompoundTypeCollection<String> {
    @Override
    protected Collection<String> createCollection() {
        return new LinkedList<>();
    }

    @Override
    protected String valueFromString(String value) {
        return value;
    }
}

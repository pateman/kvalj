package pl.pateman.kvalj.service;

import pl.pateman.kvalj.core.CollectionValueType;
import pl.pateman.kvalj.core.KValJCollection;

/**
 * Created by pateman.
 */
public interface CollectionFactory {
    KValJCollection createCollectionForValueType(CollectionValueType valueType)
            throws IllegalAccessException, InstantiationException;
}

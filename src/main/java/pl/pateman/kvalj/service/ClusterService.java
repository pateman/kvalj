package pl.pateman.kvalj.service;

import pl.pateman.kvalj.cluster.ClusterNode;
import pl.pateman.kvalj.cluster.ClusterRequestType;

/**
 * Created by pateman.
 */
public interface ClusterService {
    ClusterNode getMyself();
    ClusterNode getClusterNodeForCollectionAndKey(final String collection, final String key);
    ClusterNode getClusterNodeById(final int nodeId);
    void updateNodeInformation(final ClusterNode newNodeInformation);
    void sendToNode(final ClusterNode node, ClusterRequestType requestType, final Object data);
}

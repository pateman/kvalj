package pl.pateman.kvalj.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import pl.pateman.kvalj.cluster.ClusterEnvironment;
import pl.pateman.kvalj.cluster.ClusterNode;
import pl.pateman.kvalj.cluster.ClusterRequest;
import pl.pateman.kvalj.cluster.ClusterRequestType;
import pl.pateman.kvalj.service.ClusterService;

/**
 * Created by pateman.
 */
public class ClusterServiceImpl implements ClusterService {

    @Autowired
    private ClusterEnvironment clusterEnvironment;

    @Override
    public ClusterNode getMyself() {
        return this.clusterEnvironment.getMyself();
    }

    @Override
    public ClusterNode getClusterNodeForCollectionAndKey(String collection, String key) {
        return null;
    }

    @Override
    public ClusterNode getClusterNodeById(int nodeId) {
        return this.clusterEnvironment.getNodes().
                stream().
                filter(node -> nodeId == node.getId()).
                findFirst().orElse(null);
    }

    @Override
    public void updateNodeInformation(ClusterNode newNodeInformation) {
        if (newNodeInformation == null) {
            throw new IllegalArgumentException("A valid node needs to be provided");
        }

        this.clusterEnvironment.updateNodeInformation(newNodeInformation);
    }

    @Override
    public void sendToNode(ClusterNode node, ClusterRequestType requestType, Object data) {
        if (node == null || data == null) {
            throw new IllegalArgumentException("Valid node and data need to be provided");
        }

        final ClusterRequest request = new ClusterRequest(this.getMyself(), requestType, data);
        this.clusterEnvironment.sendData(node, request, false);
    }


}

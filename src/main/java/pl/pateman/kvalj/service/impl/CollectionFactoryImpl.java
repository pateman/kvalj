package pl.pateman.kvalj.service.impl;

import pl.pateman.kvalj.core.impl.collection.*;
import pl.pateman.kvalj.service.CollectionFactory;
import pl.pateman.kvalj.core.CollectionValueType;
import pl.pateman.kvalj.core.KValJCollection;

/**
 * Created by pateman.
 */
public class CollectionFactoryImpl implements CollectionFactory {
    @Override
    public KValJCollection createCollectionForValueType(final CollectionValueType valueType)
            throws IllegalAccessException, InstantiationException {
        Class<?> clazz = null;

        switch (valueType) {
            case STRING:
                clazz = StringCollection.class;
                break;
            case NUMERIC:
                clazz = NumericCollection.class;
                break;
            case FLOATING_POINT:
                clazz = FloatingPointCollection.class;
                break;
            case STRING_LIST:
                clazz = StringListCollection.class;
                break;
            case STRING_SET:
                clazz = StringSetCollection.class;
                break;
        }

        return (KValJCollection) clazz.newInstance();
    }
}

package pl.pateman.kvalj.service.impl;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import pl.pateman.kvalj.service.CollectionFactory;
import pl.pateman.kvalj.service.CollectionRegistry;
import pl.pateman.kvalj.core.CollectionValueType;
import pl.pateman.kvalj.core.KValJCollection;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by pateman.
 */
public class CollectionRegistryImpl implements CollectionRegistry {
    private Map<String, KValJCollection> collections;

    @Autowired
    private CollectionFactory collectionFactory;

    @PostConstruct
    public void initialize() {
        this.collections = new ConcurrentHashMap<>();
    }

    @Override
    public KValJCollection getCollection(final String name) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Invalid collection name");
        }

        return this.collections.get(name);
    }

    @Override
    public Collection<String> getAllCollectionNames() {
        return new HashSet<>(this.collections.keySet());
    }

    @Override
    public KValJCollection createCollection(final String name, final CollectionValueType valueType) {
        if (StringUtils.isBlank(name)) {
            throw new IllegalArgumentException("Invalid collection name");
        }
        if (valueType == null) {
            throw new IllegalArgumentException("Invalid collection value type");
        }

        if (this.collections.containsKey(name)) {
            throw new IllegalStateException("Collection already exists");
        }

        try {
            final KValJCollection collection = this.collectionFactory.createCollectionForValueType(valueType);
            this.collections.put(name, collection);

            return collection;
        } catch (IllegalAccessException | InstantiationException e) {
            throw new IllegalStateException("Unable to create collection");
        }
    }
}

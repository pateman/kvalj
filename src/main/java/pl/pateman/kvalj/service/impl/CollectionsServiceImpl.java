package pl.pateman.kvalj.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import pl.pateman.kvalj.service.CollectionRegistry;
import pl.pateman.kvalj.core.CollectionValueType;
import pl.pateman.kvalj.core.KValJCollection;
import pl.pateman.kvalj.service.CollectionsService;

import java.util.Collection;

/**
 * Created by pateman.
 */
public class CollectionsServiceImpl implements CollectionsService {
    @Autowired
    private CollectionRegistry collectionRegistry;

    @Override
    public void createCollection(String name, CollectionValueType valueType) {
        this.collectionRegistry.createCollection(name, valueType);
    }

    @Override
    public KValJCollection getCollection(String name) {
        final KValJCollection collection = this.collectionRegistry.getCollection(name);
        if (collection == null) {
            throw new IllegalStateException("Collection does not exist");
        }
        return collection;
    }

    @Override
    public Collection<String> listCollections() {
        return this.collectionRegistry.getAllCollectionNames();
    }

    @Override
    public Object putValue(String collection, String key, String value) {
        final KValJCollection kValJCollection = this.getCollection(collection);
        return kValJCollection.put(key, value);
    }

    @Override
    public Object appendValue(String collection, String key, String value) {
        final KValJCollection kValJCollection = this.getCollection(collection);
        return kValJCollection.append(key, value);
    }

    @Override
    public Object getValue(String collection, String key) {
        return this.getCollection(collection).get(key);
    }

    @Override
    public Object removeValue(String collection, String key) {
        return this.getCollection(collection).remove(key);
    }
}

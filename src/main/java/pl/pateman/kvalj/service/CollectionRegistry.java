package pl.pateman.kvalj.service;

import pl.pateman.kvalj.core.CollectionValueType;
import pl.pateman.kvalj.core.KValJCollection;

import java.util.Collection;

/**
 * Created by pateman.
 */
public interface CollectionRegistry {
    KValJCollection getCollection(String name);

    Collection<String> getAllCollectionNames();

    KValJCollection createCollection(String name, CollectionValueType valueType);
}

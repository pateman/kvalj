package pl.pateman.kvalj.service;

import pl.pateman.kvalj.core.CollectionValueType;
import pl.pateman.kvalj.core.KValJCollection;

import java.util.Collection;

/**
 * Created by pateman.
 */
public interface CollectionsService {
    void createCollection(final String name, final CollectionValueType valueType);
    KValJCollection getCollection(final String name);
    Collection<String> listCollections();
    Object putValue(final String collection, final String key, final String value);
    Object appendValue(final String collection, final String key, final String value);
    Object getValue(final String collection, final String key);
    Object removeValue(final String collection, final String key);
}

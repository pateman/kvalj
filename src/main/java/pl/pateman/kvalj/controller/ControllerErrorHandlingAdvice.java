package pl.pateman.kvalj.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Created by pateman.
 */
@RestControllerAdvice
public class ControllerErrorHandlingAdvice {
    @ExceptionHandler(Exception.class)
    public ResponseEntity<KValJResponse> exceptionHandler(Exception ex) {
        final KValJResponse response = new KValJResponse(KValJResponse.CODE_ERROR, ex.getMessage());
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}

package pl.pateman.kvalj.controller;

/**
 * Created by pateman.
 */
public class KValJResponse {
    public static final int CODE_OK = 0;
    public static final int CODE_ERROR = 1;

    private final int code;
    private final Object data;

    public KValJResponse(int code, Object data) {
        this.code = code;
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public Object getData() {
        return data;
    }
}

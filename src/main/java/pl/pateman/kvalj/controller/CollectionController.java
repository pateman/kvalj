package pl.pateman.kvalj.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.pateman.kvalj.core.CollectionValueType;
import pl.pateman.kvalj.core.KValJCollection;
import pl.pateman.kvalj.service.CollectionsService;

import java.util.Collection;

/**
 * Created by pateman.
 */
@RestController
@RequestMapping("/c")
public class CollectionController {

    @Autowired
    private CollectionsService collectionsService;

    @RequestMapping(method = RequestMethod.GET)
    public KValJResponse allCollections() {
        final Collection<String> listCollections = this.collectionsService.listCollections();
        return new KValJResponse(KValJResponse.CODE_OK, listCollections);
    }

    @RequestMapping(value = "/{collectionName}", method = RequestMethod.POST)
    public KValJResponse createCollection(@PathVariable final String collectionName,
                                          @RequestParam(name = "type")
                                            final CollectionValueType collectionValueType) {
        this.collectionsService.createCollection(collectionName, collectionValueType);
        return new KValJResponse(KValJResponse.CODE_OK, "");
    }

    @RequestMapping(value = "/{collectionName}", method = RequestMethod.GET)
    public KValJResponse listAllKeysInCollection(@PathVariable final String collectionName) {
        final KValJCollection collection = this.collectionsService.getCollection(collectionName);
        return new KValJResponse(KValJResponse.CODE_OK, collection.keys());
    }

    @RequestMapping(value = "/{collectionName}/{key}", method = RequestMethod.GET)
    public KValJResponse getValue(@PathVariable final String collectionName,
                                  @PathVariable final String key) {
        final Object value = this.collectionsService.getValue(collectionName, key);
        return new KValJResponse(KValJResponse.CODE_OK, value);
    }

    @RequestMapping(value = "/{collectionName}/{key}", method = RequestMethod.POST)
    public KValJResponse putInCollection(@PathVariable final String collectionName,
                                         @PathVariable final String key,
                                         @RequestParam final String value) {
        final Object originalValue = this.collectionsService.putValue(collectionName, key, value);
        return new KValJResponse(KValJResponse.CODE_OK, originalValue);
    }

    @RequestMapping(value = "/{collectionName}/{key}", method = RequestMethod.PUT)
    public KValJResponse appendValueToCollection(@PathVariable final String collectionName,
                                                 @PathVariable final String key,
                                                 @RequestParam final String value) {
        final Object originalValue = this.collectionsService.appendValue(collectionName, key, value);
        return new KValJResponse(KValJResponse.CODE_OK, originalValue);
    }

    @RequestMapping(value = "/{collectionName}/{key}", method = RequestMethod.DELETE)
    public KValJResponse removeFromCollection(@PathVariable final String collectionName,
                                              @PathVariable final String key) {
        final Object value = this.collectionsService.removeValue(collectionName, key);
        return new KValJResponse(KValJResponse.CODE_OK, value);
    }
}

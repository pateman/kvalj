package pl.pateman.kvalj.config;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import pl.pateman.kvalj.cluster.ClusterClient;
import pl.pateman.kvalj.cluster.ClusterEnvironment;
import pl.pateman.kvalj.cluster.ClusterServer;
import pl.pateman.kvalj.cluster.impl.KryoNetClusterClient;
import pl.pateman.kvalj.cluster.impl.KryoNetClusterServer;
import pl.pateman.kvalj.service.ClusterService;
import pl.pateman.kvalj.service.CollectionFactory;
import pl.pateman.kvalj.service.CollectionRegistry;
import pl.pateman.kvalj.service.impl.ClusterServiceImpl;
import pl.pateman.kvalj.service.impl.CollectionRegistryImpl;
import pl.pateman.kvalj.service.impl.CollectionFactoryImpl;
import pl.pateman.kvalj.service.CollectionsService;
import pl.pateman.kvalj.service.impl.CollectionsServiceImpl;

/**
 * Created by pateman.
 */
@Configuration
public class ApplicationBeans {
    @Bean
    public CollectionFactory collectionFactory() {
        return new CollectionFactoryImpl();
    }

    @Bean("kvaljCollectionRegistry")
    public CollectionRegistry collectionRegistry() {
        return new CollectionRegistryImpl();
    }

    @Bean
    public CollectionsService collectionsService() {
        return new CollectionsServiceImpl();
    }

    @Bean
    public ClusterService clusterService() {
        return new ClusterServiceImpl();
    }

    @Bean
    public ClusterServer clusterServer() {
        return new KryoNetClusterServer();
    }

    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public ClusterClient clusterClient() {
        return new KryoNetClusterClient();
    }

    @Bean
    public ClusterEnvironment clusterEnvironment() {
        return new ClusterEnvironment();
    }
}

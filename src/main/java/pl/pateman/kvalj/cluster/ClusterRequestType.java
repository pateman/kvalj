package pl.pateman.kvalj.cluster;

/**
 * Created by pateman.
 */
public enum ClusterRequestType {
    NODE_INFO_REQUEST,
    NODE_INFO_RESPONSE
}

package pl.pateman.kvalj.cluster;

/**
 * Created by pateman.
 */
public final class ClusterNode {
    private int id;
    private final String host;
    private final boolean myself;
    private boolean available;
    private transient ClusterClient clusterClient;

    public ClusterNode(int id, String host, boolean myself) {
        this.id = id;
        this.host = host == null ? "" : host;
        this.myself = myself;
        this.available = false;
    }

    public int getId() {
        return id;
    }

    void setId(int id) {
        this.id = id;
    }

    public String getHost() {
        return host;
    }

    public boolean isMyself() {
        return myself;
    }

    public boolean isAvailable() {
        return available;
    }

    void setAvailable(boolean available) {
        this.available = available;
    }

    ClusterClient getClusterClient() {
        return clusterClient;
    }

    void setClusterClient(ClusterClient clusterClient) {
        this.clusterClient = clusterClient;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClusterNode that = (ClusterNode) o;

        return id == that.id && host.equals(that.host);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + host.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ClusterNode{" +
                "id=" + id +
                ", host='" + host + '\'' +
                '}';
    }
}

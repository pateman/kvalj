package pl.pateman.kvalj.cluster;

/**
 * Created by pateman.
 */
public interface ClusterServer {
    void start(final int tcpPort, final int udpPort) throws Exception;
    void stop() throws Exception;
    void onRequest(final ClusterRequest request);
    int getTCPPort();
    int getUDPPort();
}

package pl.pateman.kvalj.cluster;

import java.util.Date;

/**
 * Created by pateman.
 */
public final class ClusterRequest {
    private final Date requestDate;
    private final Integer nodeId;
    private final ClusterRequestType requestType;
    private final Object data;

    public ClusterRequest(final ClusterNode issuingNode, ClusterRequestType requestType, Object data) {
        if (issuingNode == null || requestType == null || data == null) {
            throw new IllegalArgumentException("All parameters must be provided");
        }

        this.data = data;
        this.nodeId = issuingNode.getId();
        this.requestType = requestType;
        this.requestDate = new Date();
    }

    public Date getRequestDate() {
        return requestDate;
    }

    public Integer getNodeId() {
        return nodeId;
    }

    public ClusterRequestType getRequestType() {
        return requestType;
    }

    public Object getData() {
        return data;
    }
}

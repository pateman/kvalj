package pl.pateman.kvalj.cluster.impl;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import pl.pateman.kvalj.cluster.ClusterClient;
import pl.pateman.kvalj.cluster.ClusterNode;
import pl.pateman.kvalj.cluster.ClusterRequest;
import pl.pateman.kvalj.service.ClusterService;

/**
 * Created by pateman.
 */
public class KryoNetClusterClient implements ClusterClient, EnvironmentAware {
    private static final String CLUSTER_TIMEOUT_PROPERTY = "cluster.timeout";
    private static final Logger LOG = LoggerFactory.getLogger(KryoNetClusterClient.class);
    private final Client client;

    private Environment environment;

    @Autowired
    private ClusterService clusterService;

    public KryoNetClusterClient() {
        this.client = new Client();

        //  Configure Kryo.
        this.client.getKryo().register(ClusterNode.class);
        this.client.getKryo().register(ClusterRequest.class);
    }

    @Override
    public void connect(ClusterNode node, int tcpPort, int udpPort) throws Exception {
        this.client.start();

        final Integer timeout = this.environment.getProperty(CLUSTER_TIMEOUT_PROPERTY, Integer.class, 5000);
        this.client.connect(timeout, node.getHost(), tcpPort, udpPort);
        this.client.addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                if (object instanceof ClusterRequest) {
                    KryoNetClusterClient.this.onRequest((ClusterRequest) object);
                }
            }
        });
    }

    @Override
    public void disconnect() throws Exception {
        this.client.stop();
    }

    @Override
    public void sendTCP(ClusterRequest request) {
        this.client.sendTCP(request);
    }

    @Override
    public void sendUDP(ClusterRequest request) {
        this.client.sendUDP(request);
    }

    @Override
    public void onRequest(ClusterRequest request) {
        switch (request.getRequestType()) {
            case NODE_INFO_RESPONSE:
                final ClusterNode clusterNode = (ClusterNode) request.getData();
                LOG.info("Received cluster node info %s", clusterNode);
                this.clusterService.updateNodeInformation(clusterNode);
                break;
        }
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }
}

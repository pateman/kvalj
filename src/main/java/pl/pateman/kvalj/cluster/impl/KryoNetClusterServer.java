package pl.pateman.kvalj.cluster.impl;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import org.springframework.beans.factory.annotation.Autowired;
import pl.pateman.kvalj.cluster.ClusterNode;
import pl.pateman.kvalj.cluster.ClusterRequest;
import pl.pateman.kvalj.cluster.ClusterRequestType;
import pl.pateman.kvalj.cluster.ClusterServer;
import pl.pateman.kvalj.service.ClusterService;

/**
 * Created by pateman.
 */
public class KryoNetClusterServer implements ClusterServer {
    private final Server server;
    private int tcpPort;
    private int udpPort;

    @Autowired
    private ClusterService clusterService;

    public KryoNetClusterServer() {
        this.server = new Server();
    }

    @Override
    public void start(int tcpPort, int udpPort) throws Exception {
        this.server.start();

        //  Configure Kryo.
        this.server.getKryo().register(ClusterNode.class);
        this.server.getKryo().register(ClusterRequest.class);

        this.server.bind(tcpPort, udpPort);
        this.tcpPort = tcpPort;
        this.udpPort = udpPort;

        this.server.addListener(new Listener() {
            @Override
            public void received(Connection connection, Object object) {
                if (object instanceof ClusterRequest) {
                    KryoNetClusterServer.this.onRequest((ClusterRequest) object);
                }
            }
        });
    }

    @Override
    public void stop() throws Exception {
        this.server.stop();
    }

    @Override
    public void onRequest(ClusterRequest request) {
        switch (request.getRequestType()) {
            case NODE_INFO_REQUEST:
                final ClusterNode myself = this.clusterService.getMyself();
                final ClusterNode recipient = this.clusterService.getClusterNodeById(request.getNodeId());
                this.clusterService.sendToNode(recipient, ClusterRequestType.NODE_INFO_RESPONSE, myself);

                break;
        }
    }

    @Override
    public int getTCPPort() {
        return this.tcpPort;
    }

    @Override
    public int getUDPPort() {
        return this.udpPort;
    }
}

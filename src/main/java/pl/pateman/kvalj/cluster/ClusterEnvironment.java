package pl.pateman.kvalj.cluster;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by pateman.
 */
public final class ClusterEnvironment implements EnvironmentAware, ApplicationContextAware,
        ApplicationListener<EmbeddedServletContainerInitializedEvent> {
    private static final String CLUSTER_NODEID_PROPERTY = "cluster.nodeid";
    private static final String CLUSTER_NODEHOST_PROPERTY = "cluster.nodehost";
    private static final String CLUSTER_TCPPORT_PROPERTY = "cluster.tcpport";
    private static final String CLUSTER_UDPPORT_PROPERTY = "cluster.udpport";
    private static final String CLUSTER_NODES_PROPERTY = "cluster.nodes";
    public static final int INCORRECT_NODE_ID = -1;

    private final List<ClusterNode> nodes;
    private ClusterNode myself;

    private Environment environment;
    private ApplicationContext applicationContext;

    @Autowired
    private ClusterServer clusterServer;

    public ClusterEnvironment() {
        this.nodes = new ArrayList<>();
    }

    public List<ClusterNode> getNodes() {
        return new ArrayList<>(this.nodes);
    }

    public ClusterNode getMyself() {
        return this.myself;
    }

    public int getClusterTCPPort() {
        return this.clusterServer.getTCPPort();
    }

    public int getClusterUDPPort() {
        return this.clusterServer.getUDPPort();
    }

    public void updateNodeInformation(final ClusterNode newNodeInformation) {
        this.nodes.
                stream().
                filter(node -> node.getId() == INCORRECT_NODE_ID && node.getHost().equals(newNodeInformation.getHost())).
                forEach(node -> {
                    node.setId(newNodeInformation.getId());
                    node.setAvailable(true);
                });
    }

    public void sendData(final ClusterNode node, final ClusterRequest request, final boolean useUDP) {
        if (node == null || request == null) {
            throw new IllegalArgumentException("Node and request need to be provided");
        }

        if (!node.equals(this.myself) && node.getClusterClient() == null) {
            throw new IllegalStateException("Node does not have a client assigned");
        }

        if (useUDP) {
            node.getClusterClient().sendUDP(request);
        } else {
            node.getClusterClient().sendTCP(request);
        }
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    @Override
    public void onApplicationEvent(EmbeddedServletContainerInitializedEvent event) {
        //  Configure "myself".
        final Integer nodeId = this.environment.getProperty(CLUSTER_NODEID_PROPERTY, Integer.class, 1);
        final String nodeHost = this.environment.getProperty(CLUSTER_NODEHOST_PROPERTY, String.class, "127.0.0.1");
        this.myself = new ClusterNode(nodeId, nodeHost, true);

        //  Start the cluster server.
        final Integer tcpPort = this.environment.getProperty(CLUSTER_TCPPORT_PROPERTY, Integer.class, 8002);
        final Integer udpPort = this.environment.getProperty(CLUSTER_UDPPORT_PROPERTY, Integer.class, 8003);
        try {
            this.clusterServer.start(tcpPort, udpPort);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        //  Read other nodes.
        final String otherNodes = this.environment.getProperty(CLUSTER_NODES_PROPERTY, String.class, "");
        if (StringUtils.isBlank(otherNodes)) {
            return;
        }

        this.nodes.addAll(Arrays.stream(otherNodes.split(",")).map(node -> {
            final ClusterNode clusterNode = new ClusterNode(INCORRECT_NODE_ID, node, false);

            final ClusterClient clusterClient = this.applicationContext.getBean(ClusterClient.class);
            try {
                clusterClient.connect(clusterNode, tcpPort, udpPort);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            final ClusterRequest request = new ClusterRequest(this.myself, ClusterRequestType.NODE_INFO_REQUEST, "");
            clusterClient.sendTCP(request);

            clusterNode.setClusterClient(clusterClient);
            return clusterNode;
        }).collect(Collectors.toList()));

    }
}

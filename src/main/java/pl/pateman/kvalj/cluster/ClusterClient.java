package pl.pateman.kvalj.cluster;

/**
 * Created by pateman.
 */
public interface ClusterClient {
    void connect(ClusterNode node, final int tcpPort, final int udpPort) throws Exception;
    void disconnect() throws Exception;
    void sendTCP(final ClusterRequest request);
    void sendUDP(final ClusterRequest request);
    void onRequest(final ClusterRequest request);
}
